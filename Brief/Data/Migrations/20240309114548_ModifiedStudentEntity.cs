﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Brief.Data.Migrations
{
    /// <inheritdoc />
    public partial class ModifiedStudentEntity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BriefDatas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Customer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SiteGoals = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BusinessSpecifics = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Competitors = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TargetAudience = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DesignPreferences = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Brending = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Opportunities = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Navigation = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MainPage = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BudgetAndDeadlines = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AcceptanceOfWork = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Deployment = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BriefDatas", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BriefDatas");
        }
    }
}
