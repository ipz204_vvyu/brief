﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Brief.Data.Migrations
{
    /// <inheritdoc />
    public partial class _1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TargetAudience",
                table: "BriefDatas",
                newName: "Uniqueness");

            migrationBuilder.RenameColumn(
                name: "SiteGoals",
                table: "BriefDatas",
                newName: "ReadyIntegrations");

            migrationBuilder.RenameColumn(
                name: "Opportunities",
                table: "BriefDatas",
                newName: "ProblemsAndMotives");

            migrationBuilder.RenameColumn(
                name: "Navigation",
                table: "BriefDatas",
                newName: "PerfectClient");

            migrationBuilder.RenameColumn(
                name: "MainPage",
                table: "BriefDatas",
                newName: "PaidOrFree");

            migrationBuilder.RenameColumn(
                name: "DesignPreferences",
                table: "BriefDatas",
                newName: "Pages");

            migrationBuilder.RenameColumn(
                name: "Deployment",
                table: "BriefDatas",
                newName: "Niche");

            migrationBuilder.RenameColumn(
                name: "Competitors",
                table: "BriefDatas",
                newName: "MinimalisticOrDetailed");

            migrationBuilder.RenameColumn(
                name: "BusinessSpecifics",
                table: "BriefDatas",
                newName: "List");

            migrationBuilder.RenameColumn(
                name: "BudgetAndDeadlines",
                table: "BriefDatas",
                newName: "LikeAndDoNotLikeExamples");

            migrationBuilder.RenameColumn(
                name: "Brending",
                table: "BriefDatas",
                newName: "IsDeployNeeded");

            migrationBuilder.RenameColumn(
                name: "AcceptanceOfWork",
                table: "BriefDatas",
                newName: "Ideas");

            migrationBuilder.AddColumn<string>(
                name: "AdditionalGoals",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Blog",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "BrendBook",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "BrendElements",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "BudgetAndDeadlines1",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ClientMaterials",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ClientPath",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactPerson",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactStages",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Copyright",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DemographicData",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Domain",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Emotions",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Examples",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Features",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "FeaturesToContactWithClient",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "FinalGoals",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Functions",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "GrowthPoints",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "HaveWebSite",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "HelpAfterRelease",
                table: "BriefDatas",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdditionalGoals",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "Blog",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "BrendBook",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "BrendElements",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "BudgetAndDeadlines1",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "ClientMaterials",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "ClientPath",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "ContactPerson",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "ContactStages",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "Copyright",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "DemographicData",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "Domain",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "Emotions",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "Examples",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "Features",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "FeaturesToContactWithClient",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "FinalGoals",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "Functions",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "GrowthPoints",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "HaveWebSite",
                table: "BriefDatas");

            migrationBuilder.DropColumn(
                name: "HelpAfterRelease",
                table: "BriefDatas");

            migrationBuilder.RenameColumn(
                name: "Uniqueness",
                table: "BriefDatas",
                newName: "TargetAudience");

            migrationBuilder.RenameColumn(
                name: "ReadyIntegrations",
                table: "BriefDatas",
                newName: "SiteGoals");

            migrationBuilder.RenameColumn(
                name: "ProblemsAndMotives",
                table: "BriefDatas",
                newName: "Opportunities");

            migrationBuilder.RenameColumn(
                name: "PerfectClient",
                table: "BriefDatas",
                newName: "Navigation");

            migrationBuilder.RenameColumn(
                name: "PaidOrFree",
                table: "BriefDatas",
                newName: "MainPage");

            migrationBuilder.RenameColumn(
                name: "Pages",
                table: "BriefDatas",
                newName: "DesignPreferences");

            migrationBuilder.RenameColumn(
                name: "Niche",
                table: "BriefDatas",
                newName: "Deployment");

            migrationBuilder.RenameColumn(
                name: "MinimalisticOrDetailed",
                table: "BriefDatas",
                newName: "Competitors");

            migrationBuilder.RenameColumn(
                name: "List",
                table: "BriefDatas",
                newName: "BusinessSpecifics");

            migrationBuilder.RenameColumn(
                name: "LikeAndDoNotLikeExamples",
                table: "BriefDatas",
                newName: "BudgetAndDeadlines");

            migrationBuilder.RenameColumn(
                name: "IsDeployNeeded",
                table: "BriefDatas",
                newName: "Brending");

            migrationBuilder.RenameColumn(
                name: "Ideas",
                table: "BriefDatas",
                newName: "AcceptanceOfWork");
        }
    }
}
