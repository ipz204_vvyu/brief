﻿using Brief.Models;

using System.Text;

namespace Brief.Services;

public static class PdfService
{
    public static string GetBriefHtml(BriefData briefData)
    {
        var sb = new StringBuilder();
        sb.Append(@"
            <html>
                <head>
                </head>
                <body>
                    <div class='header'><h1>Бриф</h1></div>");

        sb.Append($"<div class='created-date'>Дата створення: {briefData.CreatedDate}</div>");
        sb.Append($"<div class='last-modified-date'>Дата зміни: {briefData.LastModifiedDate}</div>");

        sb.Append("<ul>");
        sb.AppendFormat("<li>Замовник: {0}</li>", briefData.Customer);
        sb.AppendFormat("<li>Контакти: {0}</li>", briefData.Contacts);

        sb.Append("<li>");
        sb.Append("Завдання сайту");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Наявність веб-сайту: {0}</li>", briefData.HaveWebSite);
        sb.AppendFormat("<li>Кінцева мета: {0}</li>", briefData.FinalGoals);
        sb.AppendFormat("<li>Додаткові цілі: {0}</li>", briefData.AdditionalGoals);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Особливості бізнесу");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Ніша: {0}</li>", briefData.Niche);
        sb.AppendFormat("<li>Точки зростання: {0}</li>", briefData.GrowthPoints);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Конкуренти");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Унікальності: {0}</li>", briefData.Uniqueness);
        sb.AppendFormat("<li>Приклади: {0}</li>", briefData.Examples);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Цільова аудиторія");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Демографічні дані: {0}</li>", briefData.DemographicData);
        sb.AppendFormat("<li>Ідеальний клієнт: {0}</li>", briefData.PerfectClient);
        sb.AppendFormat("<li>Проблеми і мотиви: {0}</li>", briefData.ProblemsAndMotives);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Уподобання в дизайні");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Побажання: {0}</li>", briefData.BrendElements);
        sb.AppendFormat("<li>Приклади: {0}</li>", briefData.LikeAndDoNotLikeExamples);
        sb.AppendFormat("<li>Настрій сайту: {0}</li>", briefData.Emotions);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Брендинг");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Брендбук: {0}</li>", briefData.BrendBook);
        sb.AppendFormat("<li>Ідеї: {0}</li>", briefData.Ideas);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Контент");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Копірайтер: {0}</li>", briefData.Copyright);
        sb.AppendFormat("<li>Матеріали клієнта: {0}</li>", briefData.ClientMaterials);
        sb.AppendFormat("<li>Платний контент: {0}</li>", briefData.PaidOrFree);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Можливості");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Можливості: {0}</li>", briefData.List);
        sb.AppendFormat("<li>Інтеграції: {0}</li>", briefData.ReadyIntegrations);
        sb.AppendFormat("<li>FAQ: {0}</li>", briefData.FeaturesToContactWithClient);
        sb.AppendFormat("<li>Блог: {0}</li>", briefData.Blog);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Навігація та карта сайту");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Шлях користувача: {0}</li>", briefData.ClientPath);
        sb.AppendFormat("<li>Сторінки: {0}</li>", briefData.Pages);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Головна сторінка");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Опис: {0}</li>", briefData.MinimalisticOrDetailed);
        sb.AppendFormat("<li>Функції: {0}</li>", briefData.Functions);
        sb.AppendFormat("<li>Контент: {0}</li>", briefData.Content);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Бюджет і терміни");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Бюджет і терміни: {0}</li>", briefData.BudgetAndDeadlines1);
        sb.AppendFormat("<li>Піддтримка: {0}</li>", briefData.HelpAfterRelease);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Прийом робіт");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Відповідальні: {0}</li>", briefData.ContactPerson);
        sb.AppendFormat("<li>Правки: {0}</li>", briefData.ContactStages);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("<li>");
        sb.Append("Запуск сайту");
        sb.Append("<ul>");
        sb.AppendFormat("<li>Деплоймент: {0}</li>", briefData.IsDeployNeeded);
        sb.AppendFormat("<li>Домен: {0}</li>", briefData.Domain);
        sb.Append("</ul>");
        sb.Append("</li>");

        sb.Append("</ul>");
        sb.Append("</body></html>");

        return sb.ToString();
    }

}
