﻿using Brief.Data;
using Brief.Models;
using Brief.Services;

using DinkToPdf;
using DinkToPdf.Contracts;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Brief.Controllers
{
    public class BriefDatasController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IConverter pdfConverter;

        public BriefDatasController(ApplicationDbContext context, IConverter pdfConverter)
        {
            _context = context;
            this.pdfConverter = pdfConverter;
        }

        // GET: BriefDatas
        [Authorize]
        public async Task<IActionResult> Index()
        {
            return View(await _context.BriefDatas.ToListAsync());
        }

        // GET: BriefDatas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BriefDatas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Customer,Contacts,HaveWebSite,FinalGoals,AdditionalGoals,Niche,GrowthPoints,Uniqueness,Examples,DemographicData,PerfectClient,ProblemsAndMotives,BrendElements,LikeAndDoNotLikeExamples,Emotions,BrendBook,Ideas,Copyright,ClientMaterials,PaidOrFree,List,ReadyIntegrations,FeaturesToContactWithClient,Blog,ClientPath,Pages,MinimalisticOrDetailed,Functions,Content,BudgetAndDeadlines1,HelpAfterRelease,ContactPerson,ContactStages,IsDeployNeeded,Domain")] BriefData briefData)
        {
            if (ModelState.IsValid)
            {
                var date = DateTime.Now;
                briefData.CreatedDate = date;
                briefData.LastModifiedDate = date;
                _context.Add(briefData);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Success));
            }
            return View(briefData);
        }

        public IActionResult Success()
        {
            return View();
        }

        // GET: BriefDatas/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var briefData = await _context.BriefDatas.FindAsync(id);
            if (briefData == null)
            {
                return NotFound();
            }
            return View(briefData);
        }

        // POST: BriefDatas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Customer,Contacts,HaveWebSite,FinalGoals,AdditionalGoals,Niche,GrowthPoints,Uniqueness,Examples,DemographicData,PerfectClient,ProblemsAndMotives,BrendElements,LikeAndDoNotLikeExamples,Emotions,BrendBook,Ideas,Copyright,ClientMaterials,PaidOrFree,List,ReadyIntegrations,FeaturesToContactWithClient,Blog,ClientPath,Pages,MinimalisticOrDetailed,Functions,Content,BudgetAndDeadlines1,HelpAfterRelease,ContactPerson,ContactStages,IsDeployNeeded,Domain")] BriefData briefData)
        {
            if (id != briefData.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var brief = _context.BriefDatas.AsNoTracking().FirstOrDefault(e => e.Id == id);
                    if (brief == null)
                        return NotFound();

                    briefData.CreatedDate = brief.CreatedDate;
                    briefData.LastModifiedDate = DateTime.Now;

                    _context.Update(briefData);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BriefDataExists(briefData.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(briefData);
        }

        // GET: BriefDatas/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var briefData = await _context.BriefDatas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (briefData == null)
            {
                return NotFound();
            }

            return View(briefData);
        }

        // POST: BriefDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var briefData = await _context.BriefDatas.FindAsync(id);
            if (briefData != null)
            {
                _context.BriefDatas.Remove(briefData);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        public async Task<IActionResult> Download(int id)
        {
            var briefData = await _context.BriefDatas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (briefData == null)
            {
                return NotFound();
            }

            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = $"Brief Result {briefData.Id}",
            };

            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = PdfService.GetBriefHtml(briefData),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "css", "PdfPage.css") },
            };

            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            var file = pdfConverter.Convert(pdf);

            return File(file, "application/pdf", "EmployeeReport.pdf");
        }

        private bool BriefDataExists(int id)
        {
            return _context.BriefDatas.Any(e => e.Id == id);
        }
    }
}
