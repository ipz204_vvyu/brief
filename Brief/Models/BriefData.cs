﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Brief.Models;

public class BriefData
{
    [Key]
    public int Id { get; set; }

    [DisplayName("Хто замовник? Це організація чи конкретна особа?")]
    public string Customer { get; set; }

    [DisplayName("Контакти. Як можна з вами з'язатися?")]
    public string? Contacts { get; set; }

    [DisplayName("Чи є у вас зараз сайт? Що вам подобається і не подобається у ньому?")]
    public string? HaveWebSite { get; set; }

    [DisplayName("Яка кінцева мета вашого сайту? (наприклад, прямі продажі, надання інформації про компанію, використання як вітрини товарів)")]
    public string? FinalGoals { get; set; }

    [DisplayName("Якщо є додаткові цілі сайтів, вкажіть їх.")]
    public string? AdditionalGoals { get; set; }

    [DisplayName("Вкажіть особливості бізнес-ніші, в якій ви працюєте. Що визначає конкуренцію в ніші?")]
    public string? Niche { get; set; }

    [DisplayName("Розкажіть про точки зростання, які можуть бути у вашого бізнесу в майбутньому.")]
    public string? GrowthPoints { get; set; }

    [DisplayName("Розкажіть про ваших конкурентів. Що унікального пропонуєте ви у порівнянні з конкурентами, які переваги мають конкуренти?")]
    public string? Uniqueness { get; set; }

    [DisplayName("Наведіть сайти конкурентів, які вам подобаються. Опишіть, що вам подобається і не подобається у сайтах конкурентів?")]
    public string? Examples { get; set; }
    [DisplayName("Визначте демографічні дані та характеристики цільової аудиторії сайту: вік, стать, географію та інтереси.")]
    public string? DemographicData { get; set; }
    [DisplayName("Якщо у вас є портрет ідеального клієнта, розкажіть про нього детальніше.")]
    public string? PerfectClient { get; set; }
    [DisplayName("Якщо у вас є проблеми і мотиви, розкажіть про них детальніше.")]
    public string? ProblemsAndMotives { get; set; }
    [DisplayName("Скажіть чи є конкретні кольори, шрифти або типи зображень, які повинні бути на сайті?")]
    public string? BrendElements { get; set; }
    [DisplayName("Наведіть приклади сайтів чи стилів у дизайні, які вам подобаються або не подобаються. Чому вони вам подобаються або не подобаються?")]
    public string? LikeAndDoNotLikeExamples { get; set; }
    [DisplayName("Який настрій має викликати дизайн вашого сайту? Перерахуйте ці емоції.")]
    public string? Emotions { get; set; }
    [DisplayName("Чи є у вас брендбук? Наскільки він актуальний, наскільки потрібно дотримуватись його вимог?")]
    public string? BrendBook { get; set; }
    [DisplayName("Чи є у вас ідеї щодо того, як сайт повинен відображати індивідуальність бренду та послання бренду? Розкажіть як саме.")]
    public string? Ideas { get; set; }
    [DisplayName("Чи є у вас копірайтер, який міг би підготувати текстовий контент для сайту?")]
    public string? Copyright { get; set; }
    [DisplayName("Чи можна використовувати на сайті інформацію з інших ваших матеріалів?")]
    public string? ClientMaterials { get; set; }
    [DisplayName("Вкажіть, чи можемо ми брати фото, ілюстрації та відео з платних стоків або тільки з безкоштовних?")]
    public string? PaidOrFree { get; set; }
    [DisplayName("Перерахуйте можливості, які мають бути на сайті. Ваш сайт має бути однією мовою чи потрібна багатомовність?")]
    public string? List { get; set; }
    [DisplayName("Чи є необхідність у готових інтеграціях або сторонніх віджетах? Що саме вас цікавить?")]
    public string? ReadyIntegrations { get; set; }
    [DisplayName("Чи потрібно додавати на сайт форми зворотного зв'язку, попапи, підписку на новини, модуль інтернет-магазину, файли для завантаження, FAQ?")]
    public string? FeaturesToContactWithClient { get; set; }
    [DisplayName("Вам потрібен блог? Додати стрічку публікацій на головну, розміщувати посилання на блог в підвалі чи шапці сайту?")]
    public string? Blog { get; set; }
    [DisplayName("Яким має бути шлях користувача (наприклад, він повинен отримати максимум інформації про нас або якнайшвидше переходити до цільової дії)?")]
    public string? ClientPath { get; set; }
    [DisplayName("Які сторінки на сайті обов'язкові? Які з них мають бути унікальними, а які відрізнятимуться лише за наповненням")]
    public string? Pages { get; set; }
    [DisplayName("Якою має бути головна сторінка: мінімалістичною чи докладною?")]
    public string? MinimalisticOrDetailed { get; set; }
    [DisplayName("Які функції потрібно реалізувати на головній сторінці (наприклад, перехід до магазину, надсилання форм, детальний опис послуг)?")]
    public string? Functions { get; set; }
    [DisplayName("Що потрібно розмістити на головній сторінці сайту?")]
    public string? Content { get; set; }
    [DisplayName("Визначте бюджет та терміни проєкту (з зазначенням конкретних етапів, які потрібно закінчити до певного часу).")]
    public string? BudgetAndDeadlines1 { get; set; }
    [DisplayName("Чи потрібна вам підтримка після запуску сайту? Яку саме, як довго потрібно надавати підтримку?")]
    public string? HelpAfterRelease { get; set; }
    [DisplayName("Визначте відповідальних за кожний етап прийому робіт? Вкажіть їх контактні дані та посади.")]
    public string? ContactPerson { get; set; }
    [DisplayName("Скажіть скільки етапів правок ви хотіли б бачити? Після яких кроків розробки сайту ви хотіли б давати зворотний зв'язок?")]
    public string? ContactStages { get; set; }
    [DisplayName("Ви хотіли б отримати сайт, готовий до запуску, або вже запущений сайт?")]
    public string? IsDeployNeeded { get; set; }
    [DisplayName("У вас є домен для сайту чи його потрібно купити? Який має бути домен, якщо його потрібно купувати?")]
    public string? Domain { get; set; }
    [DisplayName("Дата створення")]
    public DateTime CreatedDate { get; set; }
    [DisplayName("Дата редагування")]
    public DateTime LastModifiedDate { get; set; }
}